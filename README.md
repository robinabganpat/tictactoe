# Tic Tac Toe

Tic Tac Toe Multiplayer game using:
- .NET Core (2.1) SignalR
- Angular 5

Requirements to build the project:
- .NET Core SDK (https://dotnet.microsoft.com/download/dotnet-core/2.1)
- NodeJS (https://nodejs.org/en/download/)
