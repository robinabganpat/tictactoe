﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AngularTicTacToe.Models.Enums
{
    public enum CellState
    {
        Blank,
        X,
        O
    }
}
