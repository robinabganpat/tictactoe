﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AngularTicTacToe.Models.Enums
{
    public enum BoardState
    {
        Draw,
        PlayerXWin,
        PlayerOWin,
        PlayerXTurn,
        PlayerOTurn
    }
}
