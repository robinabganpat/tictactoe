﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AngularTicTacToe.Models
{
    /// <summary>
    /// Represents connected client as player. Is distinctive by its ConnectionId.
    /// </summary>
    public class Player
    {
        public Player()
        {
        }

        public Player(string connectionId)
        {
            this.ConnectionId = connectionId;
        }
        public string ConnectionId { get; set; }
    }
}
