﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AngularTicTacToe.Models
{
    /// <summary>
    /// Represents a room where PlayerX and PlayerO can play a TicTacToe game.
    /// </summary>
    public class Room
    {
        public Guid RoomId { get; set; }
        public string Name { get; set; }
        public int GamesPlayed { get; set; }
        public int PlayerXWins { get; set; }
        public int PlayerOWins { get; set; }
        public int Draws { get; set; }
        public Player PlayerX { get; set; }
        public Player PlayerO { get; set; }
        public Game CurrentGame { get; set; }

        public int PlayerCount
        {
            get
            {
                int count = 0;
                if (PlayerO != null)
                    count++;
                if (PlayerX != null)
                    count++;
                return count;
            }
        }

        /// <summary>
        /// Is given connectionId either PlayerX or PlayerO?
        /// </summary>
        /// <param name="connectionId">ConnectionId of client.</param>
        /// <returns>True if given connectionId is in this room. False otherwise.</returns>
        public bool IsInRoom(string connectionId)
        {
            return  (this.PlayerX != null && this.PlayerX.ConnectionId == connectionId) || 
                    (this.PlayerO != null && this.PlayerO.ConnectionId == connectionId);
        }
    }
}
