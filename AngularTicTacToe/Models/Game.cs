﻿using AngularTicTacToe.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AngularTicTacToe.Models
{
    /// <summary>
    /// Game object which represents the current state of the game.
    /// </summary>
    public class Game
    {
        private Random rand = new Random();

        public CellState[][] Board { get; set; }

        public int MoveCount { get; set; }

        public BoardState BoardState { get; set; }

        public Game()
        {
            ResetBoard();
        }

        /// <summary>
        /// Initialize the empty board.
        /// </summary>
        public void ResetBoard()
        {
            this.MoveCount = 0;
            this.BoardState = rand.Next(0, 2) == 0 ? BoardState.PlayerXTurn : BoardState.PlayerOTurn;
            this.Board = new CellState[3][];
            for (int i = 0; i < 3; i++)
            {
                this.Board[i] = new CellState[3];
                for (int j = 0; j < 3; j++)
                {
                    this.Board[i][j] = CellState.Blank;
                }
            }
        }

        /// <summary>
        /// Move the given player on the specified position on the board. Method checks for validity of the move and resulting next state of the board.
        /// </summary>
        /// <param name="x">X Position of the move.</param>
        /// <param name="y">Y Position of the move.</param>
        /// <param name="player">The player that made the move.</param>
        /// <returns>The new board state.</returns>
        public BoardState Move(int x, int y, CellState player)
        {
            // Move validity checks.
            if (player == CellState.Blank)
            {
                throw new InvalidOperationException("Blank is not a valid player.");
            }
            else if (this.Board[x][y] != CellState.Blank)
            {
                throw new InvalidOperationException("You are not allowed to move here. Another piece is already using this cell.");
            }
            else if(this.BoardState == BoardState.PlayerOTurn && player == CellState.X)
            {
                throw new InvalidOperationException("It is O's turn.");
            }
            else if (this.BoardState == BoardState.PlayerXTurn && player == CellState.O)
            {
                throw new InvalidOperationException("It is X's turn.");
            }
            this.Board[x][y] = player;
            this.MoveCount++;

            // Who's the winner, in case last move was a winning move.
            BoardState potentialWinner = player == CellState.O ? BoardState.PlayerOWin : BoardState.PlayerXWin;

            // Check for the columns if last move was a winning move.
            for (int i = 0; i < 3; i++)
            {
                if (this.Board[x][i] != player)
                    break;
                if (i == 2)
                {
                    return potentialWinner;
                }
            }

            // Check for the rows if the last move was a winning move.
            for (int i = 0; i < 3; i++)
            {
                if (this.Board[i][y] != player)
                    break;
                if (i == 2)
                {
                    return potentialWinner;
                }
            }

            // Check the board diagonally for a winner.
            if (x == y)
            {
                // The last move was diagonal.
                for (int i = 0; i < 3; i++)
                {
                    if (this.Board[i][i] != player)
                        break;
                    if (i == 2)
                    {
                        return potentialWinner;
                    }
                }
            }

            // Check the board anti-diagonally for a winner.
            if (x + y == 2)
            {
                for (int i = 0; i < 3; i++)
                {
                    if (this.Board[i][(2) - i] != player)
                        break;
                    if (i == 2)
                    {
                        return potentialWinner;
                    }
                }
            }

            // Check if the last move resulted in a draw.
            if (this.MoveCount == 8)
            {
                return BoardState.Draw;
            }

            // In case the last move didn't result in a win or draw, return the state in which it's the next players' turn.
            return player == CellState.O ? BoardState.PlayerXTurn : BoardState.PlayerOTurn;
        }
    }
}
