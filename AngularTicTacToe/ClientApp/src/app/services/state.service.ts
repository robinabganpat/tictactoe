import { Injectable } from '@angular/core';
import { Room } from '../models/room.model';

@Injectable()
export class State {

    public rooms: { [id: string]: Room; };
    public connectionId: string;
    
    public get roomsList() : Room[]{
        return Object.values(this.rooms);
    }

    constructor() {
        this.rooms = {};
    }

}
