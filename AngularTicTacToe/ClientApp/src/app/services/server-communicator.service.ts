import { Injectable } from '@angular/core';
import { HubConnection, HubConnectionBuilder } from '@aspnet/signalr'
import { Room } from '../models/room.model';
import { State } from './state.service';
import { delay } from 'q';
import { CellState } from '../models/cell-state.enum';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class ServerCommunicatorService {

  private hubConnection: HubConnection;
  private _state: State;
  public isInitialized = false;
  private roomsUpdatedSubject: Subject<void> = new Subject();

  public get roomsUpdated$() {
    return this.roomsUpdatedSubject.asObservable();
  }

  public get state() {
    return this._state;
  }

  constructor() {
    this._state = new State();
    this.hubConnection = new HubConnectionBuilder()
      .withUrl('https://localhost:44327/game')
      .build();

    this.hubConnection
      .start()
      .then(async () => await this.initialize())
      .catch(err => console.log('Error while starting connection: ' + err));
  }

  private async initialize(): Promise<void> {
    console.log('Connection started');

    const me = this;
    this.hubConnection.on('UpdateRooms', (rooms) => {
      this.handleUpdateRooms(rooms, me);
    });
    await this.hubConnection.invoke<string>('SendConnectionId').then(connectionId => {
      me._state.connectionId = connectionId;
    });
    this.getRooms();
    this.isInitialized = true;
  }

  public async isInitializedPromise(): Promise<void> {
    while (!this.isInitialized) {
      await delay(100);
    }
  }

  private delay(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  private handleUpdateRooms(rooms: { [id: string]: Room; }, scope: ServerCommunicatorService): { [id: string]: Room; } {
    scope._state.rooms = rooms;
    this.roomsUpdatedSubject.next();
    return rooms;
  }

  public async getRooms(): Promise<{ [id: string]: Room; }> {
    let rooms = null;
    const me = this;
    await this.hubConnection.invoke<{ [id: string]: Room; }>('SendRooms').then(rooms => {
      rooms = this.handleUpdateRooms(rooms, me);
    });
    return rooms;
  }

  public async createRoom(roomName: string): Promise<string> {
    return await this.hubConnection.invoke<string>('SendCreateRoom', roomName).then(roomId => {
      return roomId = roomId;
    });
  }

  public async makeMove(roomId: string, x: number, y: number): Promise<string> {
    return await this.hubConnection.invoke('SendMakeMove', roomId, x, y);
  }

  public async joinRoom(roomId: string, player: CellState): Promise<void> {
    return await this.hubConnection.invoke('SendJoinRoom', roomId, player).then(roomId => {
      return;
    });
  }

  public async newGame(roomId: string): Promise<void> {
    return await this.hubConnection.invoke('SendNewGame', roomId).then(_ => {
      return;
    });
  }

  public async destroyRoom(roomId: string): Promise<void> {
    return await this.hubConnection.invoke('SendDestroyRoomById', roomId).then(_ => {
      return;
    });
  }
}
