import { TestBed, inject } from '@angular/core/testing';

import { ServerCommunicatorService } from './server-communicator.service';

describe('ServerCommunicatorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ServerCommunicatorService]
    });
  });

  it('should be created', inject([ServerCommunicatorService], (service: ServerCommunicatorService) => {
    expect(service).toBeTruthy();
  }));
});
