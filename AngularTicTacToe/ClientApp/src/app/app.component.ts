import { Component } from '@angular/core';
import { ServerCommunicatorService } from './services/server-communicator.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  constructor(serverCommunicator: ServerCommunicatorService) {
    
  }
}
