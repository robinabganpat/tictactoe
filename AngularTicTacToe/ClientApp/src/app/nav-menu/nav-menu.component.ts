import { Component } from '@angular/core';
import { State } from '../services/state.service';
import { ServerCommunicatorService } from '../services/server-communicator.service';

@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.css']
})
export class NavMenuComponent {
  isExpanded = false;

  constructor(public serverCommunication: ServerCommunicatorService) {

  }

  collapse() {
    this.isExpanded = false;
  }

  toggle() {
    this.isExpanded = !this.isExpanded;
  }
}
