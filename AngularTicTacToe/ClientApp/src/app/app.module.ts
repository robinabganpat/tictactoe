import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { HomeComponent } from './home/home.component';
import { RoomComponent } from './room/room.component';
import { ServerCommunicatorService } from './services/server-communicator.service';
import { RoomResolver } from './room/room.resolver';
import { GameComponent } from './game/game.component';

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    RoomComponent,
    GameComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([
      { path: '', component: HomeComponent, pathMatch: 'full' },
      {
        path: 'room/:roomId',
        component: RoomComponent,
        resolve: {
          'room': RoomResolver
        }
      },
    ])
  ],
  providers: [
    ServerCommunicatorService,
    RoomResolver
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
