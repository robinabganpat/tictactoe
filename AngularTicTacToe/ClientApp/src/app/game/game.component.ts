import { Component, OnInit, Input } from '@angular/core';
import { CellState } from '../models/cell-state.enum';
import { BoardState } from '../models/board-state.enum';
import { ServerCommunicatorService } from '../services/server-communicator.service';
import { Room } from '../models/room.model';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css']
})
export class GameComponent implements OnInit {

  @Input() room: Room
  @Input() player: CellState;
  constructor(private serverCommunicatorService: ServerCommunicatorService) { }

  ngOnInit() {
  }

  /**
   * Return string representation of given CellState.
   * @param cellState
   */
  public getTextRepresentationOfCellState(cellState: CellState): string {
    switch (cellState) {
      case CellState.Blank:
        return "";
      case CellState.O:
        return "O";
      case CellState.X:
        return "X";
    }
  }

  /**
   *  Game has ended when either player wins or when it's a draw.
   * */
  public isGameEnded(): boolean {
    return this.room.currentGame.boardState == BoardState.Draw || this.room.currentGame.boardState == BoardState.PlayerOWin || this.room.currentGame.boardState == BoardState.PlayerXWin;
  }

  public isOurTurn(): boolean {
    return (this.room.currentGame.boardState == BoardState.PlayerOTurn && this.player == CellState.O) || (this.room.currentGame.boardState == BoardState.PlayerXTurn && this.player == CellState.X);
  }

  public isSpectating(): boolean {
    return this.player == CellState.Blank;
  }

  /**
   * Do a move and send it to the server to calculate the new state of the board.
   * @param x
   * @param y
   */
  public async selectCell(x: number, y: number): Promise<void> {
    if (this.isOurTurn()) {
      await this.serverCommunicatorService.makeMove(this.room.roomId, x, y);
    }
  }

  /**
   * Request server to start a new game in current room.
   * */
  public async newGame(): Promise<void> {
    await this.serverCommunicatorService.newGame(this.room.roomId);
  }
}
