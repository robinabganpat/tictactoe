import { CellState } from "./cell-state.enum";
import { BoardState } from "./board-state.enum";

export interface Game {
    board: CellState[][];
    moveCount: 0;
    boardState: BoardState;
}