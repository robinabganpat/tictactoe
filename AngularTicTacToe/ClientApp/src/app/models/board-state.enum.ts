export enum BoardState {
    Draw = 0,
    PlayerXWin = 1,
    PlayerOWin = 2,
    PlayerXTurn = 3,
    PlayerOTurn = 4
}