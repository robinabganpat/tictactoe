export enum CellState {
    Blank = 0,
    X = 1,
    O = 2
}