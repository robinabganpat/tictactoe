import { Player } from "./player.model";
import { Game } from "./game.model";

export interface Room{
    roomId: string;
    name: string;
    gamesPlayed: number;
    playerXWins: number;
    playerOWins: number;
    draws: number;
    playerX: Player;
    playerO: Player;
    currentGame: Game;
}