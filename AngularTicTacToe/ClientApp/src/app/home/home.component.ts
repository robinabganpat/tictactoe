import { Component } from '@angular/core';
import { ServerCommunicatorService } from '../services/server-communicator.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent {

  public roomName: string;

  constructor(private serverCommunicatorService: ServerCommunicatorService, private router: Router)
  {

  }

  public createRoom() : void {
    this.serverCommunicatorService.createRoom(this.roomName).then(roomId => {
      this.router.navigate(['/room/', roomId]);
    });
  }
}
