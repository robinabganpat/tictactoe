import { Injectable } from '@angular/core';

import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/delay';
import { Room } from '../models/room.model';
import { ServerCommunicatorService } from '../services/server-communicator.service';

@Injectable()
export class RoomResolver implements Resolve<Room> {
  constructor(private serverCommunication: ServerCommunicatorService, private router: Router) {}

  async resolve(route: ActivatedRouteSnapshot) {
    const roomId = route.paramMap.get('roomId');
    return await this.serverCommunication.isInitializedPromise().then(() => {
        const room = this.serverCommunication.state.rooms[roomId];
        if(!room)
        {
          this.router.navigate(['/']);
        }
        return room;
    });
  }
}