import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Room } from '../models/room.model';
import { Subscription } from 'rxjs/Subscription';
import { ServerCommunicatorService } from '../services/server-communicator.service';
import { CellState } from '../models/cell-state.enum';

@Component({
  selector: 'app-room',
  templateUrl: './room.component.html',
  styleUrls: ['./room.component.css']
})
export class RoomComponent implements OnInit, OnDestroy {

  private routeSubscription: Subscription;
  private roomUpdateSubscription: Subscription;

  public cellState: CellState;

  public room: Room;

  constructor(
    private route: ActivatedRoute,
    private serverCommunicatorService: ServerCommunicatorService,
    private router: Router) {
    this.roomUpdateSubscription = serverCommunicatorService.roomsUpdated$.subscribe(() => {
      this.room = serverCommunicatorService.state.rooms[this.room.roomId];
      if (this.room == undefined) {
        this.router.navigate(['/']);
        alert('This room in not available anymore.');
      }
    });
    this.routeSubscription = this.route.params.subscribe(val => {
      this.room = this.route.snapshot.data['room'];
    });
  }

  ngOnInit() {

  }

  ngOnDestroy() {
    if (this.routeSubscription) {
      this.routeSubscription.unsubscribe();
    }
    if (this.roomUpdateSubscription) {
      this.roomUpdateSubscription.unsubscribe();
    }
  }

  public get player(): CellState {
    if (!this.room)
      return CellState.Blank;
    const ourConnectionId = this.serverCommunicatorService.state.connectionId;
    if (this.room.playerO && this.room.playerO.connectionId == ourConnectionId)
      return CellState.O;
    else if (this.room.playerX && this.room.playerX.connectionId == ourConnectionId)
      return CellState.X;
    else
      return CellState.Blank;
  }

  public async destroyRoom(): Promise<void> {
    await this.serverCommunicatorService.destroyRoom(this.room.roomId);
  }

  public async select(player: CellState): Promise<void> {
    await this.serverCommunicatorService.joinRoom(this.room.roomId, player);
  }

}
