﻿using AngularTicTacToe.Models;
using AngularTicTacToe.Models.Enums;
using AngularTicTacToe.Services;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AngularTicTacToe.Hubs
{
    /// <summary>
    /// Game Hub which clients can connect to.
    /// </summary>
    public class GameHub : Hub
    {
        private readonly IRoomState _roomState;

        public GameHub(IRoomState roomState)
        {
            this._roomState = roomState;
        }

        /// <summary>
        /// Destroy all the rooms the disconnected client was part of.
        /// </summary>
        /// <param name="exception"></param>
        /// <returns></returns>
        public override async Task OnDisconnectedAsync(Exception exception)
        {
            foreach (var room in this._roomState.Rooms.Values.ToList().Where(r => (r.PlayerO != null && r.PlayerO.ConnectionId == this.Context.ConnectionId) || (r.PlayerX != null && r.PlayerX.ConnectionId == this.Context.ConnectionId)))
            {
                this._roomState.Rooms.Remove(room.RoomId.ToString(), out _);
            }
            await Clients.All.SendAsync("UpdateRooms", this._roomState.Rooms);

            await base.OnDisconnectedAsync(exception);
        }

        /// <summary>
        /// Create a new Room given the name.
        /// Sends update to all connected clients.
        /// </summary>
        /// <param name="roomName">The name of the room.</param>
        /// <returns></returns>
        public async Task<string> SendCreateRoom(string roomName)
        {
            Guid roomId = Guid.NewGuid();
            Room room = new Room()
            {
                CurrentGame = new Game(),
                GamesPlayed = 0,
                Name = roomName,
                PlayerXWins = 0,
                PlayerOWins = 0,
                Draws = 0,
                RoomId = roomId,
                PlayerO = null,
                PlayerX = null
            };
            this._roomState.Rooms[roomId.ToString()] = room;
            await SendRooms();
            return room.RoomId.ToString();
        }

        /// <summary>
        /// Sends the given Room-object by its RoomId.
        /// </summary>
        /// <param name="roomId">Unique ID of given room.</param>
        /// <returns></returns>
        public Room SendRoomById(string roomId)
        {
            Room room = this._roomState.Rooms[roomId];
            return room;
        }

        /// <summary>
        /// Destroys the given Room by it's ID.
        /// </summary>
        /// <param name="roomId">Unique ID of given room.</param>
        /// <returns></returns>
        public async Task SendDestroyRoomById(string roomId)
        {
            Room room = this._roomState.Rooms[roomId];
            if (room != null)
            {
                if (room.IsInRoom(this.Context.ConnectionId))
                {
                    this._roomState.Rooms.Remove(roomId, out _);
                    await SendRooms();
                }
                else
                {
                    throw new InvalidOperationException("You are only allowed to destroy a room when you are part of the room yourself.");
                }
            }
        }

        /// <summary>
        /// Leaves the room when connected client is part of the given room.
        /// Will destroy the room in case the last connected client left.
        /// </summary>
        /// <param name="roomId"></param>
        /// <returns></returns>
        public async Task SendLeaveRoom(string roomId)
        {
            Room room = this._roomState.Rooms[roomId];
            if (!room.IsInRoom(this.Context.ConnectionId))
            {
                throw new InvalidOperationException("You can only leave the room when you play as either PlayerX or PlayerO.");
            }
            if (room.PlayerO != null && room.PlayerO.ConnectionId == this.Context.ConnectionId)
            {
                room.PlayerO = null;
            }
            else if (room.PlayerX != null && room.PlayerX.ConnectionId == this.Context.ConnectionId)
            {
                room.PlayerX = null;
            }

            //Destroy the room if there's no one in it anymore.
            if (room.PlayerCount == 0)
                this._roomState.Rooms.Remove(roomId, out _);
            await SendRooms();
        }

        /// <summary>
        /// Send an update to all connected clients with the current rooms.
        /// </summary>
        /// <returns>Dictionary of the rooms.</returns>
        public async Task<IDictionary<string, Room>> SendRooms()
        {
            var rooms = this._roomState.Rooms;
            await Clients.All.SendAsync("UpdateRooms", rooms);
            return rooms;
        }

        /// <summary>
        /// Send the connected client its ConnectionId.
        /// </summary>
        /// <returns>ConnectionId of the client.</returns>
        public string SendConnectionId()
        {
            return this.Context.ConnectionId;
        }

        /// <summary>
        /// Attempts to join a given room.
        /// </summary>
        /// <param name="roomId">The room the client wants to join.</param>
        /// <param name="player">The player the client wants to play as.</param>
        /// <returns>The assigned player.</returns>
        public async Task<CellState> SendJoinRoom(string roomId, CellState player)
        {
            if (this._roomState.Rooms.ContainsKey(roomId))
            {
                Room room = this._roomState.Rooms[roomId];
                if (room.PlayerO == null && player == CellState.O)
                {
                    room.PlayerO = new Player(this.Context.ConnectionId);
                }
                else if (room.PlayerX == null && player == CellState.X)
                {
                    player = CellState.X;
                    room.PlayerX = new Player(this.Context.ConnectionId);
                }
                else
                    throw new InvalidOperationException("That player-spot is already taken.");

                await SendRooms();
            }

            return player;
        }

        /// <summary>
        /// Start a new game in given Room.
        /// </summary>
        /// <param name="roomId">Unique ID of the room.</param>
        /// <returns></returns>
        public async Task SendNewGame(string roomId)
        {
            Room room = this._roomState.Rooms[roomId];
            if(room != null)
            {
                if(room.IsInRoom(this.Context.ConnectionId))
                {
                    room.CurrentGame.ResetBoard();
                    await SendRooms();
                }
                else
                {
                    throw new InvalidOperationException("You are not allowed to start a new game, since you are not playing.");
                }
            }
        }

        /// <summary>
        /// Attempts to make a move by client.
        /// </summary>
        /// <param name="roomId">The room ID of the game where the move should be made.</param>
        /// <param name="x">X position of the move.</param>
        /// <param name="y">Y position of the move.</param>
        /// <returns></returns>
        public async Task SendMakeMove(string roomId, int x, int y)
        {
            Room room = this._roomState.Rooms[roomId];
            if (room.CurrentGame != null)
            {
                CellState currentPlayer = CellState.Blank;
                if (room.PlayerO != null && room.PlayerO.ConnectionId == this.Context.ConnectionId)
                {
                    currentPlayer = CellState.O;
                }
                else if (room.PlayerX != null && room.PlayerX.ConnectionId == this.Context.ConnectionId)
                {
                    currentPlayer = CellState.X;
                }
                else
                {
                    throw new InvalidOperationException("You are not allowed to make a move, since you're not part of the game.");
                }

                // Make the move and get the new state of the board.
                BoardState newState = room.CurrentGame.Move(x, y, currentPlayer);

                if (newState == BoardState.Draw)
                {
                    room.Draws++;
                }
                else if (newState == BoardState.PlayerXWin)
                {
                    room.PlayerXWins++;
                }
                else if (newState == BoardState.PlayerOWin)
                {
                    room.PlayerOWins++;
                }

                room.CurrentGame.BoardState = newState;
                await SendRooms();
            }
        }
    }
}
