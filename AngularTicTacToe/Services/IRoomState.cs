﻿using AngularTicTacToe.Models;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AngularTicTacToe.Services
{
    public interface IRoomState
    {
        ConcurrentDictionary<string, Room> Rooms { get; set; }
    }
}
