﻿using AngularTicTacToe.Models;
using AngularTicTacToe.Services;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AngularTicTacToe.Services
{
    public class RoomState : IRoomState
    {
        public ConcurrentDictionary<string, Room> Rooms { get; set; } = new ConcurrentDictionary<string, Room>();
    }
}
